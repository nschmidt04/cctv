﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ParcelTrackingView.Entities;
using ParcelTrackingView.Core;
using Newtonsoft.Json;

namespace ParcelTrackingView.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SearchController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly ISearchLog _searchLog;
        private readonly ISearchVideo _searchVideo;

        public SearchController(IConfiguration config, ISearchLog searchLog, ISearchVideo searchVideo)
        {
            _config = config;
            _searchLog = searchLog;
            _searchVideo = searchVideo;
        }

        [HttpPost]
        [Route("search")]
        public IActionResult Search([FromBody] Search search)
        {
            try
            {
                List<Parcel> searchInLog = _searchLog.SearchInLog(search);

                var jsonResult = JsonConvert.SerializeObject(searchInLog); 

                return Ok(jsonResult);
            }
            catch {
                // No se encuentran SearchId
                return NotFound();
            }
        }

        [HttpPost]
        [Route("searchVideo")]
        public IActionResult SearchVideo([FromBody] Parcel parcel)
        {
            try
            {
                string pathVideo = _searchVideo.GetVideoPath(parcel);

                var jsonResult = JsonConvert.SerializeObject(pathVideo);

                if (pathVideo != null)
                {
                    return Ok(jsonResult);
                }
                else {
                    // Video not found
                    return NotFound();
                }

            }
            catch {
                // 
                return NotFound();
            }        
        }
    }
}