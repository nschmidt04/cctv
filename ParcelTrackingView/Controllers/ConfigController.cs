﻿using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ParcelTrackingView.Entities;


namespace ParcelTrackingView.Controllers
{
    [Route("api/[controller]")]

    [ApiController]
    public class ConfigController : ControllerBase
    {
        private string _url = "Config/";


        [HttpPost]
        [Route("setUrlSettings")]
        public IActionResult SetSettings([FromBody] UrlSettings settings) {

            string settingsJson = JsonConvert.SerializeObject(settings);

            //write string to file
            System.IO.File.WriteAllText(@_url+ "urlSettings.config", settingsJson);
            return Ok();
        }

        [HttpGet]
        [Route("getUrlSettings")]
        public IActionResult GetUrlSettings()
        {
            UrlSettings settings = new UrlSettings();
            try
            {
                var jsonDataSettings = System.IO.File.ReadAllText(@_url + "urlSettings.config");
                var jsonSettings = JsonConvert.DeserializeObject<UrlSettings>(jsonDataSettings);

                return Ok(jsonSettings);
            }
            catch {

                System.IO.File.WriteAllText(@_url + "urlSettings.config", JsonConvert.SerializeObject(settings));

                return Ok();
            } 
        }

        [HttpGet]
        [Route("getMsgSettings")]
        public ActionResult<IEnumerable<MsgSettings>> GetMsgSettings()
        {
            MsgSettings msgSettings = new MsgSettings();
            try
            {
                var jsonDataMsgSettings = System.IO.File.ReadAllText(@_url + "msgSettings.config");
                var jsonMsgSettings = JsonConvert.DeserializeObject<MsgSettings>(jsonDataMsgSettings);

                return Ok(jsonMsgSettings);
            }
            catch
            {
                System.IO.File.WriteAllText(@_url + "msgSettings.config", JsonConvert.SerializeObject(msgSettings));

                return Ok();
            }
        }

        [HttpPost]
        [Route("setMsgSettings")]
        public IActionResult SetMsgSettings([FromBody] MsgSettings msgSettings)
        {

            string msgSettingsJson = JsonConvert.SerializeObject(msgSettings);

            //write string to file
            System.IO.File.WriteAllText(@_url+"msgSettings.config", msgSettingsJson);
            return Ok();
        }

        [HttpPost]
        [Route("setConfigBuilded")]
        public IActionResult SetConfigBuilded([FromBody] ConfigBuilded configBuilded)
        {

            string configBuildedJson = JsonConvert.SerializeObject(configBuilded);

            //write string to file
            System.IO.File.WriteAllText(@_url+"configBuilded.config", configBuildedJson);
            return Ok();
        }

        [HttpGet]
        [Route("getVideoSettings")]
        public IActionResult GetVideoSettings()
        {
            VideoSettings settings = new VideoSettings();
      
            List<VideoSettings> settingsList = new List<VideoSettings>();

            try
            {
                using (StreamReader r = new StreamReader(@_url + "videoSettings.config"))
                {
                   string json = r.ReadToEnd();
                   settingsList = JsonConvert.DeserializeObject<List<VideoSettings>>(json);
                }

                return Ok(settingsList);              
            }
            catch
            {

                System.IO.File.WriteAllText(@_url + "videoSettings.config", JsonConvert.SerializeObject(settingsList));

                return Ok();
            }
        }

        [HttpPost]
        [Route("setVideoSettings")]
        public IActionResult SetVideoSettings([FromBody] VideoSettings[] videoSettings)
        {

            string videoSettingsJson = JsonConvert.SerializeObject(videoSettings);

            //write string to file
            System.IO.File.WriteAllText(@_url + "videoSettings.config", videoSettingsJson);
            return Ok();
        }
    }
}