﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ParcelTrackingView.Entities;
using System.Text;
using Newtonsoft.Json;
using System.Globalization;
using Microsoft.Extensions.Configuration;

namespace ParcelTrackingView.Core
{
    public class SearchLog : ISearchLog
    {
        private readonly IConfiguration _config;

        public SearchLog(IConfiguration config)
        {
            _config = config;
        }

        public List<Parcel> SearchInLog(Search search)
        {
            List<Parcel> msg = new List<Parcel>();

            var logFilePath = _config.GetValue<string>("UrlLogFsc", string.Empty);
            var msgType = _config.GetValue<string>("typeMessage", string.Empty);
            var delimiter = _config.GetValue<string>("delimiter", "|");
            var picIndex = _config.GetValue<int>("picIndex", 0);
            var hostPicIndex = _config.GetValue<int>("hostPicIndex", 0);
            var parcelDestinationIndex = _config.GetValue<int>("parcelDestinationIndex", 0);

            var log = OpenLog(logFilePath);

            // Busco todas las lineas que contengan SearchId
            var matchAll = Array.FindAll(log, n => n.Contains(search.searchId));

            // Del resultado anterior busco las que sean mensajes de clasificacion. ej: RECV:20|
            string[] matchClasificationMsg = Array.FindAll(matchAll, n => n.Contains(msgType + delimiter));

            // recorro cada resultado anterior y obtengo de cada uno Fecha|pic|hostPic|searchId|parcelDestination
            foreach (string s in matchClasificationMsg)
            {
                Parcel parcel = new Parcel();
                DateTime parsedDate = new DateTime();

                string[] matchSplit = s.Split(delimiter);
                            
                var date = matchSplit[0].Substring(0, 23);

                StringBuilder sb = new StringBuilder(date);
                // replace caracter 19 ":" por "." - 2018-03-01 07:10:21:789
                sb.Remove(19, 1).Insert(19, ".");

                parsedDate = DateTime.Parse(sb.ToString());
                
                parcel.clasificationDate = parsedDate;
                parcel.pic = matchSplit[picIndex].Trim();
                parcel.hostPic = matchSplit[hostPicIndex].Trim();
                parcel.searchId = search.searchId;
                parcel.destination = matchSplit[parcelDestinationIndex].Trim();

                msg.Add(parcel);
            }

            return msg;
        }

        private string[] OpenLog(string urlLogFsc)
        {

            string[] log = System.IO.File.ReadAllLines(urlLogFsc);

            return log;
        }
    }
}
