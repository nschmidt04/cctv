﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ParcelTrackingView.Entities;
using System.Text;
using Newtonsoft.Json;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;

using Microsoft.Extensions.Configuration;
namespace ParcelTrackingView.Core
{
    public class SearchVideo : ISearchVideo
    {

        private readonly IConfiguration _config;

        public SearchVideo(IConfiguration config)
        {
            _config = config;
        }

        public string GetVideoPath(Parcel parcel)
        {
            var videoPath = _config.GetValue<string>("UrlVideo", string.Empty);

            var camaraId = getCameraId(parcel.destination);

            string path = videoPath + "\\" +  parcel.clasificationDate.ToString("dd_MM_yyyy");
            
            string pattern = @"(\d{1,2})_.*_(\d{4}-\d{2}-\d{2}\s\d{2}-\d{2}-\d{2})_(\d{4}-\d{2}-\d{2}\s\d{2}-\d{2}-\d{2})";

            foreach (string urlFile in Directory.EnumerateFiles(
                path, "*.*", SearchOption.AllDirectories))
            {
                RegexOptions options = RegexOptions.Multiline;
                DateTime startDate = new DateTime();
                DateTime endDate = new DateTime();
  
                foreach (Match m in Regex.Matches(urlFile, pattern, options))
                {
              
                    startDate = DateTime.ParseExact(m.Groups[2].Value, "yyyy-MM-dd HH-mm-ss", null);
                    endDate = DateTime.ParseExact(m.Groups[3].Value, "yyyy-MM-dd HH-mm-ss", null);


                    if (parcel.clasificationDate >= startDate && parcel.clasificationDate <= endDate) {

                        var pathVideo = urlFile;
                        return urlFile;
                    }
                }
            }

            return null;
        }

        private string getCameraId(string salida)
        {
            var pathUrlVideo = "Config\\videoSettings.config";
            List<VideoSettings> videoSettings = JsonConvert.DeserializeObject<List<VideoSettings>>(System.IO.File.ReadAllText(pathUrlVideo));
            
            foreach (var videoSetting in videoSettings)
            {
                if (videoSetting.Chute == salida)
                {
                    return videoSetting.CameraId;
                }
            }

            return null;
        }


    }
}
