﻿using System;
using ParcelTrackingView.Entities;

namespace ParcelTrackingView.Core
{
    public interface ISearchVideo
    {
        string GetVideoPath(Parcel parcel);
    }
}
