﻿using System.Collections.Generic;
using ParcelTrackingView.Entities;

namespace ParcelTrackingView.Core
{
    public interface ISearchLog
    {
        List<Parcel> SearchInLog(Search search);
    }
}
