import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {LoginObject} from './login-object.model';
import {Session} from '../../../core/models/session.model';
import {HttpClient} from '@angular/common/http';
/**
 * Created by nschmidt on 23/05/2019.
 */
@Injectable()
export class AuthenticationService {

  constructor(private http: HttpClient) {}

  private basePath = '/api/authenticate/';

  login(loginObj: LoginObject): Observable<Session> {
    return this.http.post<Session>(this.basePath + 'login', loginObj);
  }

  logout(): Observable<boolean> {
    return this.http.post<boolean>(this.basePath + 'logout', {});
  }
}
