import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConfigService } from '../../../core/services/config.service';
import { config, Observable } from 'rxjs';
import { MenssageSettings } from 'src/app/core/models/msgSettings.model';
import { tap } from 'rxjs/operators';
import { ConfigBuilded } from '../../../core/models/configBuilded.model';
import { ConfigBuilder } from '../configBuilder';

@Component({
  selector: 'app-msg-settings',
  templateUrl: './menssageSettings.component.html',
  styleUrls: ['./menssageSettings.component.css']
})
export class MenssageSettingsComponent implements OnInit {
  subTitle = 'Menssage Settings';
  msgSettingForm: FormGroup;
  submitted = false;

  msgConfigLoaded: Observable<MenssageSettings>;
  public configBuilder: ConfigBuilder;
  public configBuilded: ConfigBuilded;

  constructor(private formMsgSettingBuilder: FormBuilder, private configService: ConfigService) { 
    this.configBuilder = new ConfigBuilder();
  }

  ngOnInit() {

    this.msgSettingForm = this.formMsgSettingBuilder.group({
      fullMessage: ['', Validators.required],
      delimiter: ['', Validators.required],
      typeMessage: ['', Validators.required],
      pic: ['', Validators.required],
      hostPic: ['', Validators.required],
      barcode: ['', Validators.required],
      parcelDestination: ['', Validators.required]
    });

    this.msgConfigLoaded = this.configService.getMsgSettings().pipe(
      tap(msgConfigLoaded => this.msgSettingForm.patchValue(msgConfigLoaded))
    );
  }

    // Menssage Settings
    msgSettingSubmit(): void {
      this.submitted = true;
  
      if ( this.msgSettingForm.invalid ) {
        return;
      }
  
      let msgSettings: MenssageSettings = new MenssageSettings();
      msgSettings.fullMessage = this.msgSettingForm.controls.fullMessage.value;
      msgSettings.pic = this.msgSettingForm.controls.pic.value;
      msgSettings.hostPic = this.msgSettingForm.controls.hostPic.value;
      msgSettings.delimiter = this.msgSettingForm.controls.delimiter.value;
      msgSettings.barcode = this.msgSettingForm.controls.barcode.value;
      msgSettings.typeMessage = this.msgSettingForm.controls.typeMessage.value;
      msgSettings.parcelDestination = this.msgSettingForm.controls.parcelDestination.value;
  
  
      this.configBuilded = this.configBuilder.configBuilder( msgSettings );
  
      this.configService.setMsgSettings( msgSettings );
      this.configService.setConfigBuilded( this.configBuilded );
  
      alert(JSON.stringify(this.configBuilded));
      }

      refresh() {
         this.msgConfigLoaded = this.configService.getMsgSettings().pipe(
         tap(msgConfigLoaded => this.msgSettingForm.patchValue(msgConfigLoaded))
    );
      }

}
