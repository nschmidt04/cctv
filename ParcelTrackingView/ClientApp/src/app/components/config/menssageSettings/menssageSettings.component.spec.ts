import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenssageSettingsComponent } from './menssageSettings.component';

describe('MsgSettingsComponent', () => {
  let component: MenssageSettingsComponent;
  let fixture: ComponentFixture<MenssageSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenssageSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenssageSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
