import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UrlSettings } from '../../../core/models/urlSettings.model';
import { ConfigService } from '../../../core/services/config.service';
import { tap } from 'rxjs/operators';
import { config, Observable } from 'rxjs';

@Component({
  selector: 'app-url-settings',
  templateUrl: './urlSettings.component.html',
  styleUrls: ['./urlSettings.component.css']
})
export class UrlSettingsComponent implements OnInit {
  title = 'Url Settings';
  configLoaded: Observable<UrlSettings>;
  settingForm: FormGroup;
  submitted = false;


  constructor(private formSettingsBuilder: FormBuilder, private configService: ConfigService) { }

  ngOnInit() {
    this.settingForm = this.formSettingsBuilder.group({
      urlLogFsc: ['', Validators.required],
      urlVideo: ['', Validators.required]
    });


    this.configLoaded = this.configService.getUrlSettings().pipe(
      tap(configLoaded => this.settingForm.patchValue(configLoaded))
    );

  }

   // Settings
   settingSubmit(): void {
    this.submitted = true;

    if ( this.settingForm.invalid ) {
      return;
    }

    let urlSettings: UrlSettings = new UrlSettings();
    urlSettings.urlLogFsc = this.settingForm.controls.urlLogFsc.value;
    urlSettings.urlVideo = this.settingForm.controls.urlVideo.value;

    this.configService.setUrlSettings( urlSettings );
 
    }

    refresh() {
      this.configLoaded = this.configService.getUrlSettings().pipe(
        tap(configLoaded => this.settingForm.patchValue(configLoaded))
      );
    }

}
