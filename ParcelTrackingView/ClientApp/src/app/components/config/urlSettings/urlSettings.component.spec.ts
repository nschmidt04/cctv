import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UrlSettingsComponent } from './urlSettings.component';

describe('UrlSettingsComponent', () => {
  let component: UrlSettingsComponent;
  let fixture: ComponentFixture<UrlSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UrlSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UrlSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
