import { ConfigBuilded } from '../../core/models/configBuilded.model';
import { MenssageSettings } from '../../core/models/msgSettings.model';

export class ConfigBuilder  {


    configBuilder(configBuilder: MenssageSettings) {


      const splitfullMessage = configBuilder.fullMessage.split(configBuilder.delimiter);
      let configBuilded: ConfigBuilded = new ConfigBuilded();


      splitfullMessage.forEach((item, index) => {

      const itemTrim = item.trim();

      if (configBuilder.typeMessage === itemTrim) {
        configBuilded.typeMessageIndex = index;
      }
      if (configBuilder.pic.toString() === itemTrim) {
        configBuilded.picIndex = index;
      }
      if (configBuilder.hostPic.toString() === itemTrim) {
        configBuilded.hostPicIndex = index;
      }
      if (configBuilder.barcode === itemTrim) {
        configBuilded.barcodeIndex = index;
      }
      if (configBuilder.parcelDestination.toString() === itemTrim) {
        configBuilded.parcelDestinationIndex = index;
    }
    });

      return configBuilded;
      }

}
