import { Component, OnInit } from '@angular/core';
import { VideoSettings } from '../../../core/models/videoSettings.model';
import { ConfigService } from '../../../core/services/config.service';
import { config } from 'rxjs';


@Component({
  selector: 'app-video-settings',
  templateUrl: './videoSettings.component.html',
  styleUrls: ['./videoSettings.component.css']
})
export class VideoSettingsComponent implements OnInit {

  title = 'Video Settings';
  settingList: VideoSettings[];
  editField: any;
  constructor(private configService: ConfigService) { }


  ngOnInit() {
    this.configService.getVideoSettings().subscribe( data => { this.settingList = data; });
  }

  updateList(id: number, property: string, event: any) {

    const editField = event.target.textContent.trim();
    this.settingList[id][property.trim()] = editField.trim();
  }

  remove(id: number) {
    this.settingList.splice(id, 1);
  }

  add() {

      let s = new VideoSettings();
      s.cameraId = '-';
      s.chute = '-';
      s.ubication = '-';

      this.settingList.push(s);
  }

  confirm() {
    this.configService.setVideoSettings(this.settingList);
  }

  refresh() {
    this.configService.getVideoSettings().subscribe( data => { this.settingList = data; });
  }

}