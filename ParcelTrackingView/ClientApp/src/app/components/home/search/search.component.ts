import { Component, OnInit, ViewChild } from '@angular/core';
import { Search } from '../../../core/models/search.model';
import { SearchService } from '../../../core/services/search.service';
import { Parcel } from '../../../core/models/parcel.model';
import { SearchShowComponent } from '../search-show/search-show.component';
import { MatPaginator } from '@angular/material/paginator';
import { AlertPromise } from 'selenium-webdriver';


export interface Type {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})


export class SearchComponent implements OnInit {

  title = 'ParcelTrackingView';
  searchId: any;
  dateSearch: string;
  search: Search;
  selectedTypeSearch: string;
  public searchShowComponent: SearchShowComponent;

  displayedColumns: string[] = ['Date', 'Pic', 'HostPic', 'SearchId', 'Destination', 'Actions'];
  Parcels: Parcel[];
  Parcelss: Parcel[];
  @ViewChild(SearchShowComponent) searchShow: SearchShowComponent;
 
  /*
  typeSearch: Type[] = [
    {value: '0', viewValue: 'BARCODE'},
    {value: '1', viewValue: 'PIC'},
    {value: '2', viewValue: 'HOSTPIC'}
  ];
*/

  constructor(private searchService: SearchService) {
    this.search = new Search();

  }


  ngOnInit() {
  }


  // Search Click
  SearchClick() {

      this.search.searchId = this.searchId;
     // this.search.typeSearch = this.typeSearch[this.selectedTypeSearch].viewValue;
      this.search.dateSearch = this.dateSearch;

      let a = this.searchService.getParcel( this.search ).subscribe( data => { this.Parcels = data; });

      //this.searchShow.m(a);

  }

  // Go Video Click
  GoVideo(parcel) {

      this.searchService.getVideo(parcel);
  }



}
