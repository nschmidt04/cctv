import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Routing
import { AppRoutingModule } from './app-routing.module';

import { HttpClientModule} from '@angular/common/http';

// Core
import { CoreModule } from './core/core.module';

// Service
import { ConfigService } from './core/services/config.service';
import { SearchService } from './core/services/search.service';


// Components
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { NavComponent } from './components/nav/nav.component';

// Components Config
import { ConfigComponent } from './components/config/config.component';
import { UrlSettingsComponent } from './components/config/urlSettings/urlSettings.component';
import { MenssageSettingsComponent } from './components/config/menssageSettings/menssageSettings.component';
import { VideoSettingsComponent } from './components/config/videoSettings/videoSettings.component';

// Components Home
import { SearchComponent } from './components/home/search/search.component';
import { DialogVideoComponent } from './components/home/dialog-video/dialog-video.component';
import { SearchShowComponent } from './components/home/search-show/search-show.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

// Forms
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

// Angular Material
import { AngularMaterialModule } from './modules/angular-material/angular-material.module';

// Angular FlexLayout
import { FlexLayoutModule } from '@angular/flex-layout';
import { HomeComponent } from './components/home/home.component';



@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    NavComponent,
    ConfigComponent,
    LoginComponent,
    UrlSettingsComponent,
    MenssageSettingsComponent,
    SearchShowComponent,
    DialogVideoComponent,
    HomeComponent,
    VideoSettingsComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    CoreModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    AngularMaterialModule,
    ToastrModule.forRoot()
  ],
  providers: [
    ConfigService,
    SearchService
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
