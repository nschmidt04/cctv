export class Parcel {

    public clasificationDate: Date;
    public pic: string;
    public hostPic: string;
    public searchId: string;
    public parcelDestination: string;
}