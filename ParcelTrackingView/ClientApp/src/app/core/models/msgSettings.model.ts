export class MenssageSettings {
    public fullMessage: string;
    public delimiter: string;
    public typeMessage: string;
    public pic: number;
    public hostPic: number;
    public barcode: string;
    public parcelDestination: number;
}