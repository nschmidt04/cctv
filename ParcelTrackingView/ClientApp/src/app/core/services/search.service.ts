
import { Search } from '../models/search.model';
import { Parcel } from '../models/parcel.model';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';



const httpOptions = {
    headers: new HttpHeaders({"Content-Type": "application/json"})
};
const apiUrl: string = "https://localhost:44350/api/search/";



@Injectable()
export class SearchService {

    constructor(private http: HttpClient) { }


    public getParcel(search: Search): Observable<Parcel[]> {

        const http$: Observable<Parcel[]> = this.http.post<Parcel[]>(apiUrl + "search", search, httpOptions);
        http$.subscribe(
            res => console.log("HTTP response", res),
            err => console.log("HTTP Error", err),
            () => console.log("HTTP request completed.")
        );

        return http$;
    }

    public getVideo(parcel: Parcel): Observable<string> {

        const http$: Observable<string> = this.http.post<string>(apiUrl + "searchVideo", parcel, httpOptions);
        http$.subscribe(
            res => console.log("HTTP response", res),
            err => console.log("HTTP Error", err),
            () => console.log("HTTP request completed.")
        );

        return http$;
    }
}



