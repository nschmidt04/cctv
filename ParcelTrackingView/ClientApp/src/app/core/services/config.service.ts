import { ConfigBuilded } from '../models/configBuilded.model';
import { UrlSettings } from '../models/urlSettings.model';
import { VideoSettings } from '../models/videoSettings.model';
import { MenssageSettings } from '../models/msgSettings.model';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';



const httpOptions = {
    headers: new HttpHeaders({"Content-Type": "application/json"})
};
const apiUrl: string = "https://localhost:44350/api/config/";

@Injectable()
export class ConfigService {

    constructor(private http: HttpClient, private toastrService: ToastrService) { }

    getUrlSettings(): Observable<UrlSettings> {
        const http$: Observable<UrlSettings> = this.http.get<UrlSettings>(apiUrl + "getUrlSettings")
        http$.subscribe(
            res => console.log("HTTP response", res),
            err => console.log("HTTP Error", err),
            () => console.log("HTTP request completed.")
        );
        return http$;
    }

    setUrlSettings(settings: UrlSettings): Observable<UrlSettings> {
        const http$: Observable<UrlSettings> = this.http.post<UrlSettings>(apiUrl + "setUrlSettings", settings, httpOptions);
        http$.subscribe(
            res => this.toastrService.success('Saved Successfully'), 
            err => this.toastrService.error('Error'), 
            () => console.log("HTTP request completed.")
        );
        return http$;
    }

    getMsgSettings(): Observable<MenssageSettings> {
      const http$: Observable<MenssageSettings> = this.http.get<MenssageSettings>(apiUrl + "getMsgSettings")
        http$.subscribe(
            res => console.log("HTTP response", res),
            err => console.log("HTTP Error", err),
            () => console.log("HTTP request completed.")
        );
        return http$;
    }

    setMsgSettings(advSettings: MenssageSettings): Observable<MenssageSettings> {
        const http$: Observable<MenssageSettings> = this.http.post<MenssageSettings>(apiUrl + "setMsgSettings", advSettings, httpOptions);
        http$.subscribe(
            res => this.toastrService.success('Saved Successfully'), 
            err => this.toastrService.error('Error'), 
            () => console.log("HTTP request completed.")
        );
        return http$;
    }

    setConfigBuilded(configBuilded: ConfigBuilded): Observable<ConfigBuilded> {
        const http$: Observable<ConfigBuilded> = this.http.post<ConfigBuilded>(apiUrl + "setConfigBuilded", configBuilded, httpOptions);
        http$.subscribe(
            res => this.toastrService.success('Saved Successfully'), 
            err => this.toastrService.error('Error'), 
            () => console.log("HTTP request completed.")
        );
        return http$;
    }

    
    getVideoSettings(): Observable<VideoSettings[]> {
        const http$: Observable<VideoSettings[]> = this.http.get<VideoSettings[]>(apiUrl + "getVideoSettings")
        http$.subscribe(
            res => console.log("HTTP response", res),
            err => console.log("HTTP Error", err),
            () => console.log("HTTP request completed.")
        );
        return http$;
    }
  
    setVideoSettings(videoSettings: VideoSettings[]): Observable<VideoSettings[]> {
        const http$: Observable<VideoSettings[]> = this.http.post<VideoSettings[]>(apiUrl + "setVideoSettings", videoSettings, httpOptions);
        http$.subscribe(
            res => this.toastrService.success('Saved Successfully'), 
            err => this.toastrService.error('Error'), 
            () => console.log("HTTP request completed.")
        );
        return http$;
    }
}
