﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelTrackingView.Entities
{
    public class VideoSettings
    {
        public string CameraId { get; set; }
        public string Chute { get; set; }
        public string Ubication { get; set; }
    }
}
