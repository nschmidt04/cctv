﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelTrackingView.Entities
{
    public class MsgSettings
    {
        // Clasification Mensagge 
        public string fullMessage { get; set; }
        public string delimiter { get; set; }
        public string typeMessage { get; set; }
        public int pic { get; set; }
        public int hostPic { get; set; }
        public string barcode { get; set; }
        public int parcelDestination { get; set; }
    }
}


