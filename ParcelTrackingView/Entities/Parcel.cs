﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelTrackingView.Entities
{
    public class Parcel
    {
        public DateTime clasificationDate { get; set; }
        public string pic { get; set; }
        public string hostPic { get; set; }
        public string searchId { get; set; }
        public string destination { get; set; } 

    }
}
