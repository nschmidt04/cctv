﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelTrackingView.Entities
{
    public class ConfigBuilded
    {
        // Mensagge Position  

        public int typeMessageIndex { get; set; }
        public int picIndex { get; set; }
        public int hostPicIndex { get; set; }
        public int barcodeIndex { get; set; } 
        public int parcelDestinationIndex { get; set; }

    }
}

