﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelTrackingView.Entities
{
    public class UrlSettings
    {
        public string UrlLogFsc { get; set; }
        public string UrlVideo { get; set; }
    }
}
