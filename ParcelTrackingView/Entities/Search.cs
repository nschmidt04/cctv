﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParcelTrackingView.Entities
{
    public class Search
    {
        public string typeMessage { get; set; }
        public string searchId { get; set; }
        public DateTime date { get; set; }

    }
}
