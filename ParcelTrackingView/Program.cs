using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ParcelTrackingView.Entities;


namespace ParcelTrackingView
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();

        }


        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
            .ConfigureAppConfiguration((hostingContext, config) =>
            {
                config.SetBasePath(Directory.GetCurrentDirectory());
                config.AddJsonFile(
                    "config/configBuilded.config", optional: false, reloadOnChange: false);
                config.AddJsonFile(
                    "config/msgSettings.config", optional: false, reloadOnChange: false);
                config.AddJsonFile(
                    "config/urlSettings.config", optional: false, reloadOnChange: false);
            })
            .CaptureStartupErrors(true) // the default
           .UseSetting("detailedErrors", "true")
            .UseStartup<Startup>();
    }
    
}
