//Author. Schmidt Nicolas Ariel
//
///////////////////////////////////////////////
//
<--urlSettings.config-->
In this file we store the paths of FSC logs and video

<----------------------------------------->

<-msgSettings.config->
In this file we only store the selected classification message for the construction of the configBuilder.config

<----------------------------------------->

<-configBuilded.config->
In this file we store the positions of typeMessage, pic, hostPic, barcode and parcelDestination that are in the original message delimited by the delimiter

<----------------------------------------->

<-videoSettings.config->
In this file we store the relationship between the camera the output and the distance of the output to the last photocell